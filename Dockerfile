FROM maven:3.6.3-jdk-8
COPY target/demo-0.0.1-SNAPSHOT.jar /demo.jar
CMD ["java", "-jar", "target/demo-0.0.1-SNAPSHOT.jar"]